<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    
   
</head>
<body>
    
        <?php
            session_start();

            if (isset($_SESSION['username'])) {
                echo "Hello, " . $_SESSION['username'] . "<br>";
                echo '<div class="logout-btn-container">
                        <form action="./login.php" method="post">
                            <input type="submit" value="Logout">
                            <input type="hidden" name="logout" value="true">
                        </form>
                      </div>';
            } else {
                $usernameValue = '';
                if (isset($_POST['logout']) || isset($_POST['username'])) {
                    $usernameValue = $_POST['username'] ?? '';
                }
                echo '
                <form id="loginForm" action="login.php?'.uniqid().'" method="post" autocomplete="off">
                    <label for="username">Username:</label>
                    <input type="text" name="username" value="' . $usernameValue . '" required>
                    <label for="password">Password:</label>
                    <input type="password" name="password" required>
                    <input type="submit" value="Login">
                </form>';
            }
            ?>

</body>
</html>